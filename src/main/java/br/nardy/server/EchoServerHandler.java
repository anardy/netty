package br.nardy.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

/**
 * 
 * @author anardy
 * 
 *         All Netty servers require the following:
 * 
 *         At least one ChannelHandler - This component implements the server's
 *         processing of data received from the client - its business logic.
 * 
 *         Because Echo server will respond to incoming messages, it will need
 *         to implement interface ChannelInboundHandler
 */

// Indicates that a ChannelHandler can be safely shared by multiple channels
@Sharable
public class EchoServerHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		ByteBuf in = (ByteBuf) msg;
		// Writes the received message to the sender without flushing the outbound messages.
		System.out.println("Server received: " + in.toString(CharsetUtil.UTF_8));
		
		ctx.write(in);
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		// Flushes pending messages to the remote peer and closes the channel
		ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
	}

	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		// Prints the exception stack trace
		cause.printStackTrace();
		// Closes the channel
		ctx.close();
	}

}
