package br.nardy.client;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.util.CharsetUtil;

/**
 * 
 * @author anardy
 *
 * Business logic
 */

// Marks this class as one whose instances can be shared among channels
@Sharable
public class EchoClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
	
	public void channelActive(ChannelHandlerContext ctx) {
		// When notified that the channel is active, sends a message
		ctx.writeAndFlush(Unpooled.copiedBuffer("Hello", CharsetUtil.UTF_8));
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
		// Logs a dump of the received message
		System.out.println("Client received: " + msg.toString(CharsetUtil.UTF_8));
	}
	
	// On exception, logs the erro and closes channel
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}
	
}
